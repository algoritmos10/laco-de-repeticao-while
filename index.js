 var salario = 1000;

while (salario < 5000){
    salario += 200;
    console.log("Meu salário ainda é de = " + salario);
}

console.log(".")

console.log("Laço de Repetição WHILE")
/* Faça uma função para exibir os números de 1 a 100*/

function mostra1a10(){
    let i = 1
    while(i<11){
        console.log(`Número ${i}`)
        i++
    }
}
    mostra1a10()

    console.log(".");
    /* Chico tem 1,50m e cresce 2cm por ano, enquanto juca tem 1,10m e cresce 3cm por ano. Construir uma função para calcular
     quantos anos serão necessários para que juca seja maior que chico */

    function ex1(){
        console.log("EXERCÍCIO 1")
        let chico = 1.5
        let juca = 1.1
        let tempo = 0
        while(juca<chico){
            juca += 0.03
            chico += 0.02
            tempo++
        }
            console.log(tempo+ " anos")
    }
    ex1()

console.log(".")
/* Didi possui 52 anos de idade, e seus filhos possuem 10 e 12 anos, respectivamente. Faça uma função para determinar quando a idade
 de Didi será igual a soma das idades dos seus filhos.*/

function ex2(){
    console.log("EXERCÍCIO 2")
    let didi, f1, f2
    didi = 52
    f1 = 10
    f2 = 12
    while(didi > f1+f2){
        didi++
        f1++
        f2++
    }
console.log(didi+ " anos")
}
ex2()


console.log(".")
/* As idades de bob e renata são respectivamente 42 e 17 anos. Faça uma função para determinar quando bob terá o dobro da idade de renata. */

function ex3(){
    console.log("EXERCÍCIO 3")
    let bob, renata
    bob = 42
    renata = 17
    renata2 = 17
    while(bob > renata+renata2){
        bob++
        renata++
        renata2++
    }
console.log(bob+ " anos")
}
ex3()

console.log(".")
/* Supondo que a população de um país A seja da oredm de 90.000.000 de habitantes com uma taxa anual de crescimento de 3% e que a população 
de um país B seja, aproximadamente, de 200.000.000 de habitantes com uma taxa anual de crescimento de 1,5%, fazer um algoritmo que calcule
 e escreva o número de anos necessários para que a população do país A ultrapasse ou iguale a população do país B, mantidas essas taxas de
  crescimento. */

function ex4(){
    console.log("EXERCÍCIO 4")
    let paísA = 90.000000
    let paísB = 200.000000
    let tempo = 0
    while(paísA<paísB){
        paísA += 0.03*paísA
        paísB += 0.015*paísB
        tempo++
    }
console.log(tempo+ " anos")
}
ex4()

console.log(".")
/* Um determinado material radioativo perde metade da sua massa a cada 50 segundos. Dada a massa inicial, em gramas, fazer um algoritmo que
 determine o tempo necessário para que essa massa se torne menor do que 0,5 gramas. Escreva a massa inicial, a massa final e o tempo calculado
  em horas, minutos e segundos.*/

function seg_em_hr(seg){
    let hora = (seg - seg%3600)/3600
    let resto = seg%3600
    let min = (resto-resto%60)/60
    let s = resto%60
    let horário = hora+":"+min+":"+s
    return horário
}

function ex5(){
    console.log("EXERCÍCIO 5")
    let massa_inicial = prompt ("Massa Inicial?")
    let massa = Number (massa_inicial)
    let tempo = 0
    while(massa > 0.5){
        massa /= 2
        tempo += 50
    }
console.log(`Massa Inicial: ${massa_inicial}
Massa Final: ${massa}
Tempo: ${seg_em_hr(tempo)}`)
}
ex5()
